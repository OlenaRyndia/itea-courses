//connection variables
const darkmodeButton = document.querySelector('[data-darkmode]');
const darkMenu = document.querySelector('[data-dark]');
const slideSwitch = document.querySelector('[data-slide]');
//checked switch theme button
darkmodeButton.addEventListener('change', () => {
    if (this.checked) {
    } else {
        darkMenu.classList.toggle('dark');
    }
});
//slide switch theme button slide out
slideSwitch.addEventListener('mouseover', () => {
        slideSwitch.classList.add('slide');
});
//slide switch theme button slide in
slideSwitch.addEventListener('mouseleave', () => {
    slideSwitch.classList.remove('slide');
});