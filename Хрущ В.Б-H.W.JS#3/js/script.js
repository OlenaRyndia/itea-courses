//we create variables and get data
const a = 10;
const b = parseInt(prompt('Задача 1: Введіть число 10, або якесь інше'));
const num = parseInt(prompt('Задача 2: Введіть номер сезону року'));
const min = parseInt(prompt('Задача 3: Введіть число від 0 до 59'));
let result = '';
//task 1 If the variable a is equal to 10, then print 'True', otherwise print 'False'
if (a === b) {
    document.write('<p>Задача 1: Ви ввели 10 і це вірно</p>');
} else {
    document.write('<p>Задача 1: Ви ввели щось інше, не 10</p>');
}
/*task 2 Determine in which quarter of an hour this number falls
(In the first, second, third or fourth)*/
switch (num) {
    case (1):
        result = '<p>Задача 2: зима</p>';
        break;
    case (2):
        result = '<p>Задача 2: весна</p>';
        break;
    case (3):
        result = '<p>Задача 2: літо</p>';
        break;
    case (4):
        result = '<p>Задача 2: осінь</p>';
        break;
    default:
        document.write('<p>Задача 2: Введені невірні дані !</p>');
        break;
}
document.write(result);
/*task 3 The variable num can take 4 values: 1, 2, 3 or 4. If it has the value '1',
then we write the variable result as 'winter', if it has the value '2' - 'spring', etc.
Solve the problem using switch-case*/
if (min < 15 && min >= 0) {
    document.write('<p>Задача 3: Перша чверть години</p>');
} else if (min < 30 && min >= 0) {
    document.write('<p>Задача 3: Друга чверть години</p>');
} else if (min < 45 && min >= 0) {
    document.write('<p>Задача 3: Третя чверть години</p>');
} else if (min <= 59 && min >= 0) {
    document.write('<p>Задача 3: Четверта чверть години</p>');
} else {
    document.write('<p>Задача 3: Ану перевір що ти там вводиш!!</p>');
}

//header for geometric shapes
document.write('<h3>Геометричні фігури</h3>')

//rhomb
for(let a = 0; a < 10; a++){
    for(let i =  a ; i < 5; i++){
        document.write('&nbsp;');
    }
    for(let s = a + 1 ; s > 0 && s <= 5; s-- ){
        document.write('*');
    }
    for(let i =  a ; i < 10 && i >= 5; i--){
        document.write('&nbsp;');
    }
    for(let s = a; s >= 5 && s < 10; s++ ){
        document.write('*');
    }
    document.write('<br>');
}

//triangle
for(let a = 0; a < 10; a++){
    for(let i =  a ; i < 10; i++){
        document.write('&nbsp;');
    }
    for(let s = a + 1 ; s > 0; s-- ){
        document.write('*');
    }
    document.write('<br>');
}

//rectangle
for(let a = 0; a < 10; a++){
    if (a === 0 || a === 9) {
        for(let i =  0 ; i < 6; i++){
            document.write('*');
        }
    }
    if (a >= 1 && a < 9) {
        for(let i =  0 ; i < 10; i++){
            if (i === 0 || i === 9) {
                document.write('*');
            }
            if (i >= 1 && i < 9) {
                document.write('&nbsp;');
            }
        }
    }
    document.write('<br>');
}