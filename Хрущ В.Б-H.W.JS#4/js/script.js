//create variables and get data
const outContent = document.querySelector('[data-content]');
const n = parseInt(prompt('Для створення масиву введіть кількість елементів. Тільки ж не багато, десь до 30'));

//exercise 1
const exercise1 = ['a', 'b', 'c'].concat([1, 2, 3]);

//exercise 2
const arrExercise2 = ['a', 'b', 'c'];
const exercise2 = arrExercise2.push(1, 2, 3);

//exercise 3
const exercise3 = [1, 2, 3].reverse();

//exercise 4
const arrExercise4 = [1, 2, 3];
const exercise4 = arrExercise4.unshift(4, 5, 6);

//exercise 5
const arrExercise5 = [1, 2, 3];
const exercise5 = arrExercise5.push(4, 5, 6);

//exercise 6
const arrExercise6 = ['js', 'css', 'jq'];
const exercise6 = arrExercise6[0];

//exercise 7
const exercise7 = [1, 2, 3, 4, 5].slice(0, 3);

//exercise 8
const arrExercise8 = [1, 2, 3, 4, 5];
const exercise8 = arrExercise8.splice(1, 2);

//exercise 9
const arrExercise9 = [1, 2, 3, 4, 5];
const exercise9 = arrExercise9.splice(2,0, 10);

//exercise 10
const exercise10 = [3, 4, 1, 2, 7].sort((a, b) => a - b);

//exercise 11
const exercise11 = ['Привіт, ', 'світ', '!'].join('');

//exercise 12
const arrExercise12 = new Array(1, 2, 3, 4, 5);
const arrExercise12_1 = [1, 2, 3, 4, 5];

//exeprcise 13
const arrExercise13 = ['a', 'b', 'c', 'd'];

//exercise 14
function arrExercise14 (number) {
    let arr;
    if (number <= 30) {
        return arr = Array.from({length: number}, (e, i) => i + 1);
    } else {
        alert('Ти точно ввів що просять?');
        location.assign(location.href);
    }
}
const exercise14 = arrExercise14(n);

//exercise15. Вирішив максимально скоротити код, і вийшов ніндзя код. (Обережно, іронія! Багато тих, хто намагався піти шляхом ніндзя. Мало тих, кому це вдалося. Коротка змінна зникає у коді наче ніндзя у лісі)
const exerNinj = exercise14.map((el, e) => (e % 2 === 1) ? [e] = `<span class="red">${[el]}</span>` : [e] = `<p>${[el]}</p>`);

//data output to the page
const list = document.createElement('ol');
list.innerHTML = `
    <li>Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.</li>
    <p>Рішення: ${exercise1}</p>
    <li>Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.</li>
    <p>Рішення: ${arrExercise2}</p>
    <li>Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].</li>
    <p>Рішення: ${exercise3}</p>
    <li>Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.</li>
    <p>Рішення: ${arrExercise4}</p>
    <li>Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.</li>
    <p>Рішення: ${arrExercise5}</p>
    <li>Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.</li>
    <p>Рішення: ${exercise6}</p>
    <li>Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].</li>
    <p>Рішення: ${exercise7}</p>
    <li>Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].</li>
    <p>Рішення: ${arrExercise8}</p>
    <li>Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].</li>
    <p>Рішення: ${arrExercise9}</p>
    <li>Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.</li>
    <p>Рішення: ${exercise10}</p>
    <li>Дан масив з елементами 'Привіт, ', 'світ' і '!'. Необхідно вивести на екран фразу 'Привіт, світ!'.</li>
    <p>Рішення: ${exercise11}</p>
    <li>Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.</li>
    <p>Рішення: Сбосіб №1 "arr = new Array(${arrExercise12})", Сбосіб №2 "arr = [${arrExercise12_1}]"</p>
    <li>Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.</li>
    <p>Рішення: '${arrExercise13[0]} + ${arrExercise13[1]}, ${arrExercise13[2]} + ${arrExercise13[3]}'</p>
    <li>
        Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач створіть масив на ту кількість елементів, яку передав користувач. у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.
    </li>
    <p>Рішення: ${exercise14}</p>
    <li>Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.</li>
    <div class="exercise15"><p>Рішення:</p> ${exerNinj}</div>
    `;
outContent.append(list);
