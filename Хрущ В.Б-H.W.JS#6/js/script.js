//an object with a method of filling and outputting information
//можна додавати будь-яку кількість ключів(значень), через цикл буде запит на ввід інформації і її подальший вивід.
// якщо додати ще декілька вкладених об'єктів, вони теж будуть оброблені
const doc = {
    header: '',
    body: '',
    footer: '',
    date: '',
    addition: {
        header: '',
        body: '',
        footer: '',
        date: '',
    },
    create:  () => {
        for (let key in doc) {
            if (typeof(doc[key]) === 'object') {
                for (let i in doc[key]) {
                    let a = prompt('введіть ' + i + ' для додатка документу', '').trim();
                    if (a !== '' && a !== null) {
                        doc[key][i] = a;
                    } else {
                        alert('введені невірні данні, тепер будеш вводити все заново');
                        document.location.reload();
                        break;
                    }
                }
            } else if (key === 'create' || key === 'output') {
                break;
            } else {
                let b = prompt('введіть ' + key + ' для основного документу', '').trim();
                if (b !== '' && b !== null) {
                    doc[key] = b;
                } else {
                    alert('введені невірні данні, тепер будеш вводити все заново');
                    document.location.reload();
                    break;
                }
            }
        }
    },
    output: () => {
        for (let key in doc) {
            if (typeof(doc[key]) === 'object') {
                document.write(`<p class="name__${key}">${key}:</p>`);
                for (let i in doc[key]) {
                    if (i === 'header') {
                        document.write(`<h3 class="nested__${i}">${doc[key][i]}</h3>`);
                    } else if (i === 'body'){
                        document.write(`<p class="nested__${i}">${doc[key][i]}</p>`);
                    } else if (i === 'footer'){
                        document.write(`<p class="nested__${i}">${doc[key][i]}</p>`);
                    } else {
                        document.write(`<p class="nested__${i}">${doc[key][i]}</p>`);
                    }
                }
            } else if (key === 'create' || key === 'output') {
                break;
            } else {
                if (key === 'header') {
                    document.write(`<h1 class="main__${key}">${doc[key]}</h1>`);
                } else if (key === 'body'){
                    document.write(`<p class="main__${key}">${doc[key]}</p>`);
                } else if (key === 'footer'){
                    document.write(`<p class="main__${key}">${doc[key]}</p>`);
                } else {
                    document.write(`<p class="main__${key}">${doc[key]}</p>`);
                }
            }
        }
    },
}

doc.create();
doc.output();
