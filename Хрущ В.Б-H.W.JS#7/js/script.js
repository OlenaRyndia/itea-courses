// змінні для виводу інформації
const outFirstTask = document.querySelector('[data-first-task]');
const outSecondTask = document.querySelector('[data-second-task]');
const outThirdTask = document.querySelector('[data-third-task]');
// задача 1
class Worker {
    constructor(name, surName, rate, days) {
        this.name = name;
        this.surName = surName;
        this.rate = rate;
        this.days = days;
    }
    getSalary() {
        return this.rate * this.days;
    }
}
// створення екземпляра класу
const worker1 = new Worker('Entony', 'Riht', 1850, 21);
// вивід інформації на сторінку
const resultFirstTask = document.createElement('div');
resultFirstTask.innerHTML = `Вивід результату методу getSalary(): Працівник ${worker1.name} має зарплатню <span>${worker1.getSalary()}</span>`;
outFirstTask.append(resultFirstTask);
// задача 2
class MyString {
    constructor (string) {
        this.string = string;
    }
    reverse() {
        return this.string.split('').reverse().join('');
    }
    ucFirst() {
        if (!this.string) return this.string;
        return this.string[0].toUpperCase() + this.string.slice(1);
    }
    ucWords () {
        if (!this.string) return this.string;
        return this.string.split(' ').map((word) => word[0].toUpperCase() + word.substring(1)).join(' ');
    }
}
// створення екземпляра класу
const myString = new MyString('test data testing string');
// вивід інформації на сторінку
const resultSecondTask = document.createElement('div');
resultSecondTask.innerHTML = `Введена строка <span>'${myString.string}'</span>, Вивід результату роботи методу reverse(): <span>${myString.reverse()}</span>, Вивід результату роботи методу ucFirst(): <span>${myString.ucFirst()}</span>, Вивід результату роботи методу ucWords(): <span>${myString.ucWords()}</span>`;
outSecondTask.append(resultSecondTask);
// задача 3, 4
class Human {
    constructor (name , age){
        this.name = name;
        this.age = age;
        this.totalRating = 0;
        Human.container.push(this);
    }
    static container = [];
    static sortYear() {
        return Human.container.slice().sort((a, b) => a.age - b.age);
    }
    rating () {
        this.totalRating += 1;
    }
    showRating () {
        return `${this.name} має загальний рейтинг ${this.totalRating}`;
    }
}
// створення екземпляра класу
const human = new Human('Ralf', 21);
const human1 = new Human('Dodik', 67);
const human2 = new Human('David', 14);
const human3 = new Human('Shrek', 114);
const human4 = new Human('Carl', 45);
// виклик методу рівня екземпляра класу
human4.rating();
human4.rating();
human4.rating();
// вивід інформації на сторінку
const resultThirdTask = document.createElement('div');
resultThirdTask.innerHTML = `Введені дані <span>"${Human.container.map((i) => `ім'я:${i.name} вік:${i.age} рейтинг:${i.totalRating} `)}"</span> Результат роботи методу(функції) сортування за віком <span>${Human.sortYear().map((i) => `ім'я:${i.name} вік:${i.age} рейтинг:${i.totalRating} `)}</span> який являється на рівні функції-конструктора, Результат роботи методу(функції)рівня екземпляра, вивід рейтинга: <span>${human4.showRating()}</span>`;
outThirdTask.append(resultThirdTask);
console.log(Human.sortYear());
